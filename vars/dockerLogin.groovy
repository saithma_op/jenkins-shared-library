#!/usr/bin/env groovy

import com.example.Docker

def call(String credentialID) {
    return new Docker(this).dockerLogin(credentialID)
}
